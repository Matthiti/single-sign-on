# Single sign-on backend

This is the backend of the roelink.eu single sign-on. The frontend can be found [here](https://gitlab.com/Matthiti/sso-frontend).

## Requirements

- `Go >= 1.19`
- `MySQL` instance

## Install

- Copy the contents of `env.example` to `.env` and complement the values
- Generate an RSA key pair: https://gist.github.com/ygotthilf/baa58da5c3dd1f69fae9
- Store the keys in separate files, and reference them from the `.env` file (`JWT_PRIVATE_KEY` and `JWT_PUBLIC_KEY`)

## Run

- `go run cmd/main.go`
