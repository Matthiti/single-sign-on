package main

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"single-sign-on/logger"
	"single-sign-on/server"
)

var rootCmd = &cobra.Command{
	Short: "Run the SSO backend",
	Long:  "Run the SSO backend",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		port := viper.GetUint("port")
		server.Start(port)
	},
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		logger.Fatal("Running the root command failed", err)
	}
}

func init() {
	viper.AutomaticEnv()
	viper.SetConfigFile(".env")
	if err := viper.ReadInConfig(); err != nil {
		logger.Warn("Could not read .env file", err)
	}

	rootCmd.PersistentFlags().UintP("port", "p", 8000, "The port used")
	_ = viper.BindPFlag("port", rootCmd.PersistentFlags().Lookup("port"))
}
