package middleware

import (
	"context"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/exp/slices"
	"net/http"
	"single-sign-on/auth"
	"single-sign-on/database"
	"single-sign-on/httperror"
	"single-sign-on/logger"
	"single-sign-on/model"
	"strings"

	"github.com/go-chi/render"
)

func LogMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger.LogRequest(r)
		next.ServeHTTP(w, r)
	})
}

var noAuthResources = map[string][]string{
	"POST": {
		"/api/v1/auth/login",
		"/api/v1/auth/register",
		"/api/v1/auth/refresh",
		"/api/v1/auth/request_password_reset",
		"/api/v1/auth/reset_password",
		"/api/v1/notifications",
	},
}

func JWTMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !resourceNeedsAuthentication(r) {
			next.ServeHTTP(w, r)
			return
		}
		header := r.Header.Get("X-Authorization")
		if header == "" {
			render.Status(r, 401)
			render.JSON(w, r, httperror.New("X-Authorization header missing"))
			return
		}

		if len(header) <= len("Bearer ") {
			render.Status(r, 401)
			render.JSON(w, r, httperror.New("Invalid token"))
			return
		}

		tokenString := header[len("Bearer "):]
		token, err := auth.ParseToken(tokenString)
		if err != nil {
			render.Status(r, 401)
			render.JSON(w, r, httperror.New("Invalid token"))
			return
		}

		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			render.Status(r, 401)
			render.JSON(w, r, httperror.New("Invalid token"))
			return
		}

		var user model.User
		if database.DB.Where("uuid = ?", claims["uuid"]).First(&user).RecordNotFound() {
			render.Status(r, 401)
			render.JSON(w, r, httperror.New("Invalid token"))
			return
		}

		if user.BlockedAt != nil {
			render.Status(r, 403)
			render.JSON(w, r, httperror.New("Your account has been blocked"))
			return
		}

		ctx := context.WithValue(r.Context(), "user", user)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func resourceNeedsAuthentication(r *http.Request) bool {
	if strings.HasPrefix(r.URL.String(), "/static") {
		return false
	}

	resources, ok := noAuthResources[r.Method]
	return !(ok && slices.Contains(resources, r.URL.String()))
}
