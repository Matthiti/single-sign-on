package auth

import (
	"crypto/rsa"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/viper"
	"single-sign-on/model"
	"strings"
	"time"

	"github.com/go-chi/jwtauth"
)

const JwtIssuer = "sso.roelink.eu"
const JwtExpireMinutes = 60

var token *jwtauth.JWTAuth
var verificationKey *rsa.PublicKey

func InitializeJWT() error {
	signBytes := []byte(convertNewline(viper.GetString("JWT_PRIVATE_KEY")))
	signKey, err := jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		return err
	}

	verifyBytes := []byte(convertNewline(viper.GetString("JWT_PUBLIC_KEY")))
	verificationKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	if err != nil {
		return err
	}

	token = jwtauth.New("RS256", signKey, verificationKey)
	return nil
}

func GenerateJWTToken(user model.User) (string, error) {
	domainKeys := make([]string, len(user.DomainAccess))
	for i := range user.DomainAccess {
		domainKeys[i] = user.DomainAccess[i].Key
	}

	_, token, err := token.Encode(
		jwtauth.Claims{
			"uuid":    user.UUID,
			"role":    user.Role.Name,
			"domains": domainKeys,
			"exp":     time.Now().UTC().Add(time.Minute * JwtExpireMinutes).Unix(),
			"iat":     time.Now().UTC().Unix(),
			"iss":     JwtIssuer,
		},
	)
	return token, err
}

func ParseToken(tokenString string) (*jwt.Token, error) {
	return jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, errors.New(fmt.Sprintf("Unexpected signing method: %v", token.Header["alg"]))
		}
		return verificationKey, nil
	})
}

func convertNewline(str string) string {
	return strings.NewReplacer(
		"\\n", "\n",
	).Replace(str)
}
