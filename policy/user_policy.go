package policy

import (
	"net/http"

	"github.com/gofrs/uuid"
)

func AllUsersExtendedView(r *http.Request) bool {
	return isAdmin(r)
}

func GetUser() bool {
	return true
}

func Me(_ *http.Request) bool {
	return true
}

func CreateUser(r *http.Request) bool {
	return isAdmin(r)
}

func UpdateUser(r *http.Request, uuid uuid.UUID) bool {
	return isAdmin(r) || getUser(r).UUID == uuid
}

func UpdateUserRole(r *http.Request) bool {
	return isAdmin(r)
}

func DeleteUser(r *http.Request, uuid uuid.UUID) bool {
	return isAdmin(r) || getUser(r).UUID == uuid
}

func UpdatePassword(r *http.Request, uuid uuid.UUID) bool {
	return isAdmin(r) || getUser(r).UUID == uuid
}

func UpdateProfileImage(r *http.Request, uuid uuid.UUID) bool {
	return isAdmin(r) || getUser(r).UUID == uuid
}

func DeleteProfileImage(r *http.Request, uuid uuid.UUID) bool {
	return isAdmin(r) || getUser(r).UUID == uuid
}

func BlockUser(r *http.Request) bool {
	return isAdmin(r)
}

func GetPreferences(r *http.Request, uuid uuid.UUID) bool {
	return getUser(r).UUID == uuid
}

func UpdatePreferences(r *http.Request, uuid uuid.UUID) bool {
	return getUser(r).UUID == uuid
}
