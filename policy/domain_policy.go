package policy

import "net/http"

func AllDomains(_ *http.Request) bool {
	return true
}

func AllDomainsExtended(r *http.Request) bool {
	return isAdmin(r)
}

func GetDomain(_ *http.Request) bool {
	return true
}

func GetDomainExtended(r *http.Request) bool {
	return isAdmin(r)
}

func CreateDomain(r *http.Request) bool {
	return isAdmin(r)
}

func UpdateDomain(r *http.Request) bool {
	return isAdmin(r)
}

func DeleteDomain(r *http.Request) bool {
	return isAdmin(r)
}

func UpdateDomainImage(r *http.Request) bool {
	return isAdmin(r)
}

func DeleteDomainImage(r *http.Request) bool {
	return isAdmin(r)
}
