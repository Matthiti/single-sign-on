package policy

import (
	"net/http"
	"single-sign-on/model"
)

func getUser(r *http.Request) model.User {
	return r.Context().Value("user").(model.User)
}

func isAdmin(r *http.Request) bool {
	return getUser(r).Role.Name == "admin"
}
