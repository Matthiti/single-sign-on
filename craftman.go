package main

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"single-sign-on/command"
	"single-sign-on/logger"
)

func init() {
	viper.SetConfigFile(".env")
	if err := viper.ReadInConfig(); err != nil {
		logger.Warn("Could not read .env file", err)
	}
}

func main() {
	rootCmd := &cobra.Command{Use: "craftman"}
	rootCmd.AddCommand(command.CreateAdminCmd)
	if err := rootCmd.Execute(); err != nil {
		logger.Fatal("Running the root command failed", err)
	}
}
