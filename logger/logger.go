package logger

import (
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

func init() {
	err := createDirIfNotExist("logs")
	if err != nil {
		Fatal("Cannot create log directory", err)
	}

	f, err := os.OpenFile("logs/"+strconv.FormatInt(time.Now().Unix(), 10)+".log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0755)
	if err != nil {
		Fatal("Cannot open log file", err)
	}

	mw := io.MultiWriter(os.Stdout, f)
	log.SetOutput(mw)
}

func Log(message string) {
	log.Println(message)
}

func Fatal(message string, err error) {
	log.Fatalf("%s: %s", message, err)
}

func Warn(message string, err error) {
	log.Printf("[WARN] %s: %s", message, err)
}

func LogRequest(r *http.Request) {
	log.Printf("%s %s", r.Method, r.URL)
}

// CreateDirIfNotExist creates a given directory if it does not
// already exist.
func createDirIfNotExist(dir string) error {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.MkdirAll(dir, 0755)
		if err != nil {
			return err
		}
	}
	return nil
}
