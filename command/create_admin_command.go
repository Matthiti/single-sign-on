package command

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"single-sign-on/auth"
	"single-sign-on/database"
	"single-sign-on/logger"
	"single-sign-on/model"
)

var CreateAdminCmd = &cobra.Command{
	Use:   "admin:create",
	Short: "Create an admin account",
	Long:  "Create an admin account",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		firstName := viper.GetString("firstname")
		for firstName == "" {
			firstName, _ = input("First name: ")
		}

		lastName := viper.GetString("lastname")
		for lastName == "" {
			lastName, _ = input("Last name: ")
		}

		email := viper.GetString("email")
		for email == "" {
			email, _ = input("Email: ")
		}

		password := viper.GetString("password")
		for password == "" {
			password, _ = input("Password: ")
		}

		hashedPassword, err := auth.HashPassword(password)
		if err != nil {
			output("Something went wrong, please try again later")
			return
		}

		err = database.InitConnection()
		if err != nil {
			logger.Fatal("Cannot connect to database", err)
		}

		var adminRole model.Role
		database.DB.Where("name = 'admin'").Find(&adminRole)

		user := model.User{
			Firstname: firstName,
			Lastname:  lastName,
			Email:     email,
			Password:  &hashedPassword,
			RoleID:    adminRole.ID,
		}

		database.DB.Create(&user)

		preferences := model.DefaultPreferences()
		preferences.UserID = user.ID
		database.DB.Create(&preferences)

		output(fmt.Sprintf("Admin user with ID %d created successfully!", user.ID))
	},
}

func init() {
	CreateAdminCmd.PersistentFlags().StringP("firstname", "f", "", "The first name")
	_ = viper.BindPFlag("firstname", CreateAdminCmd.PersistentFlags().Lookup("firstname"))

	CreateAdminCmd.PersistentFlags().StringP("lastname", "l", "", "The last name")
	_ = viper.BindPFlag("lastname", CreateAdminCmd.PersistentFlags().Lookup("lastname"))

	CreateAdminCmd.PersistentFlags().StringP("email", "e", "", "The email")
	_ = viper.BindPFlag("email", CreateAdminCmd.PersistentFlags().Lookup("email"))

	CreateAdminCmd.PersistentFlags().StringP("password", "p", "", "The password")
	_ = viper.BindPFlag("password", CreateAdminCmd.PersistentFlags().Lookup("password"))
}
