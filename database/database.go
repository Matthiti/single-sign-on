package database

import (
	"errors"
	"fmt"
	"github.com/spf13/viper"
	"single-sign-on/database/migration"
	"single-sign-on/logger"
	"single-sign-on/model"

	"github.com/jinzhu/gorm"
	// sqlite3 driver
	// _ "github.com/jinzhu/gorm/dialects/sqlite"
	// mysql driver
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Migration interface {
	Name() string
	Migrate(db *gorm.DB)
	Rollback(db *gorm.DB)
}

type SchemaMigration struct {
	Version string `gorm:"primary_key"`
}

var DB *gorm.DB

func InitConnection() error {
	engine := viper.GetString("DB_ENGINE")
	url, err := getURL(engine)

	if err != nil {
		return err
	}

	DB, err = gorm.Open(engine, url)
	if err != nil {
		return err
	}

	DB = DB.Set("gorm:auto_preload", true)

	// Automigrate all models
	DB.AutoMigrate(&model.Role{})
	DB.AutoMigrate(&model.Preferences{})
	DB.AutoMigrate(&model.User{})
	DB.AutoMigrate(&model.Domain{})
	DB.AutoMigrate(&model.InviteToken{})
	DB.AutoMigrate(&model.Notification{})
	DB.AutoMigrate(&SchemaMigration{})

	migrate()

	return nil
}

func getURL(engine string) (string, error) {
	switch engine {
	case "mysql":
		return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=UTC",
			viper.GetString("DB_USERNAME"),
			viper.GetString("DB_PASSWORD"),
			viper.GetString("DB_HOST"),
			viper.GetString("DB_PORT"),
			viper.GetString("DB_TABLE")), nil
	case "sqlite3":
		return viper.GetString("DB_FILE"), nil
	case "postgres":
		return fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s",
			viper.GetString("DB_HOST"),
			viper.GetString("DB_PORT"),
			viper.GetString("DB_USERNAME"),
			viper.GetString("DB_TABLE"),
			viper.GetString("DB_PASSWORD")), nil
	default:
		return "", errors.New("unknown database engine")
	}
}

func migrate() {
	migrations := []Migration{
		migration.Migration2{}, migration.Migration3{}, migration.Migration4{}, migration.Migration5{}, migration.Migration6{},
	}

	for _, m := range migrations {
		if DB.Where("version = ?", m.Name()).First(&SchemaMigration{}).RecordNotFound() {
			logger.Log("Migrating " + m.Name())
			m.Migrate(DB)
			DB.Create(&SchemaMigration{m.Name()})
		}
	}
}
