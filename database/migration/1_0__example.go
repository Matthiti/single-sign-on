package migration

import "github.com/jinzhu/gorm"

type Migration1 struct{}

func (m Migration1) Name() string {
	return "1_0__example"
}

func (m Migration1) Migrate(_ *gorm.DB) {
	// Empty
}

func (m Migration1) Rollback(_ *gorm.DB) {
	// Empty
}
