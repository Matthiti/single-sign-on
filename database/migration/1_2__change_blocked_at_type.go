package migration

import (
	"single-sign-on/model"

	"github.com/jinzhu/gorm"
)

type Migration3 struct{}

func (m Migration3) Name() string {
	return "1_2__change_blocked_at_type"
}

func (m Migration3) Migrate(db *gorm.DB) {
	db.Model(&model.User{}).ModifyColumn("blocked_at", "int")
}

func (m Migration3) Rollback(_ *gorm.DB) {
	// Empty
}
