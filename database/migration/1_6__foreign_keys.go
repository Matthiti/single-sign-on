package migration

import (
	"github.com/jinzhu/gorm"
	"single-sign-on/model"
)

type Migration6 struct{}

func (m Migration6) Name() string {
	return "1_6__foreign_keys"
}

func (m Migration6) Migrate(db *gorm.DB) {
	db.Exec("ALTER TABLE users CHANGE role_id old_role_id int")
	db.Exec("ALTER TABLE users ADD role_id int unsigned")
	db.Exec("UPDATE users SET role_id = CAST(old_role_id AS unsigned)")
	db.Exec("ALTER TABLE users ADD CONSTRAINT fk_users_role_id FOREIGN KEY (role_id) REFERENCES roles(id)")
	db.Exec("ALTER TABLE users DROP COLUMN old_role_id")

	db.Model(&model.Notification{}).AddForeignKey("user_uuid", "users(uuid)", "CASCADE", "CASCADE")
	db.Model(&model.Notification{}).AddForeignKey("domain_id", "domains(id)", "CASCADE", "CASCADE")

	db.Exec("ALTER TABLE preferences CHANGE user_id old_user_id int")
	db.Exec("ALTER TABLE preferences ADD user_id int unsigned")
	db.Exec("UPDATE preferences SET user_id = CAST(old_user_id AS unsigned)")
	db.Exec("ALTER TABLE preferences ADD CONSTRAINT fk_preferences_user_id FOREIGN KEY (user_id) REFERENCES users(id)")
	db.Exec("ALTER TABLE preferences DROP COLUMN old_user_id")
}

func (m Migration6) Rollback(_ *gorm.DB) {
	// Empty
}
