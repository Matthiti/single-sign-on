package migration

import (
	"github.com/jinzhu/gorm"
)

type Migration5 struct{}

func (m Migration5) Name() string {
	return "1_5__password_null"
}

func (m Migration5) Migrate(db *gorm.DB) {
	db.Exec("ALTER TABLE users MODIFY COLUMN password VARCHAR(255)")
}

func (m Migration5) Rollback(db *gorm.DB) {
	db.Exec("ALTER TABLE users MODIFY COLUMN password VARCHAR(255) NOT NULL")
}
