package migration

import (
	"single-sign-on/model"

	"github.com/jinzhu/gorm"
)

type Migration4 struct{}

func (m Migration4) Name() string {
	return "1_3__role_fk"
}

func (m Migration4) Migrate(db *gorm.DB) {
	db.Model(&model.User{}).AddForeignKey("role_id", "roles(id)", "RESTRICT", "RESTRICT")
	db.Model(&model.InviteToken{}).AddForeignKey("role_id", "roles(id)", "RESTRICT", "RESTRICT")
}

func (m Migration4) Rollback(db *gorm.DB) {
	db.Model(&model.User{}).RemoveForeignKey("role_id", "roles(id)")
	db.Model(&model.InviteToken{}).RemoveForeignKey("role_id", "roles(id)")
}
