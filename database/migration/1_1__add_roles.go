package migration

import (
	"single-sign-on/model"

	"github.com/jinzhu/gorm"
)

type Migration2 struct{}

func (m Migration2) Name() string {
	return "1_1__add_roles"
}

func (m Migration2) Migrate(db *gorm.DB) {
	db.FirstOrCreate(&model.Role{}, model.Role{Name: "admin"})
	db.FirstOrCreate(&model.Role{}, model.Role{Name: "user"})
}

func (m Migration2) Rollback(db *gorm.DB) {
	db.Delete(model.Role{}, "name = ?", "admin")
	db.Delete(model.Role{}, "name = ?", "user")
}
