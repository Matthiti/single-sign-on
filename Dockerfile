#####################################
## STEP 1: Build executable binary ##
#####################################
# Pin to older version because of https://github.com/golang/go/issues/68976
FROM golang:1.22-alpine AS builder

RUN apk update && apk add --no-cache git

RUN mkdir -p /app

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 go build cmd/main.go

#####################################
##   STEP 2: Build a small image   ##
#####################################
FROM alpine:latest 

VOLUME /app/logs

WORKDIR /app

COPY --from=builder /app/main main

EXPOSE 8000

CMD ["/app/main"]