package test

import (
	"crypto/rand"
	"fmt"
	"testing"
	"time"

	"golang.org/x/crypto/argon2"
)

// Literal copy of the params struct in auth/password_hash.go
type params struct {
	memory      uint32
	iterations  uint32
	parallelism uint8
	saltLength  uint32
	keyLength   uint32
}

func TestArgon2Params(t *testing.T) {
	// Try to keep it under 500 ms
	p := params{
		memory:      64 * 1024,
		iterations:  2,
		parallelism: 4,
		saltLength:  16,
		keyLength:   32,
	}

	salt, _ := generateRandomBytes(p.saltLength)

	start := time.Now().UnixNano()
	argon2.IDKey([]byte("password"), salt, p.iterations, p.memory, p.parallelism, p.keyLength)
	stop := time.Now().UnixNano()

	fmt.Printf("Start: %d\nStop: %d\nDuration: %d ms\n", start, stop, (stop-start)/1000000)
}

func generateRandomBytes(n uint32) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)

	return b, err
}
