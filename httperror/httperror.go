package httperror

type HTTPError struct {
	Message string `json:"message"`
}

func New(message string) HTTPError {
	return HTTPError{message}
}
