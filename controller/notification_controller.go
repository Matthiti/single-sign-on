package controller

import (
	"net/http"
	"single-sign-on/database"
	"single-sign-on/httperror"
	"single-sign-on/mailer"
	"single-sign-on/model"
	"single-sign-on/util"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/gofrs/uuid"
)

func AllNotifications(w http.ResponseWriter, r *http.Request) {
	user := r.Context().Value("user").(model.User)

	var notifications []model.Notification
	database.DB.Where("user_uuid = ?", user.UUID).Order("created_at desc").Find(&notifications)

	render.JSON(w, r, notifications)
}

func GetNotification(w http.ResponseWriter, r *http.Request) {
	user := r.Context().Value("user").(model.User)

	idString := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Notification not found"))
		return
	}

	var notification model.Notification
	if database.DB.Where("user_uuid = ?", user.UUID).Where("id = ?", id).First(&notification).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Notification not found"))
		return
	}

	render.JSON(w, r, notification)
}

func CreateNotification(w http.ResponseWriter, r *http.Request) {
	header := r.Header.Get("X-Notification-Token")

	if header == "" {
		notAuthorized(w, r)
		return
	}

	var domain model.Domain
	if database.DB.Where("notification_token = ?", util.GetSHA256Sum(header)).First(&domain).RecordNotFound() {
		notAuthorized(w, r)
		return
	}

	var notification model.Notification
	err := parse(&notification, r)
	if err != nil || notification.UserUUID == uuid.Nil {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("Invalid notification"))
		return
	}

	var user model.User
	if database.DB.Where("uuid = ?", notification.UserUUID).First(&user).RecordNotFound() {
		render.Status(r, 409)
		render.JSON(w, r, httperror.New("Unknown user"))
		return
	}

	notification.CreatedAt = time.Now().Unix()
	notification.Read = false
	notification.Domain = domain

	database.DB.Create(&notification)
	if err = mailer.SendNotification(user, notification); err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot send email"))
		return
	}
	render.JSON(w, r, notification)
}

func ReadNotification(w http.ResponseWriter, r *http.Request) {
	type input struct {
		Read bool `json:"read"`
	}

	idString := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Notification not found"))
		return
	}

	var in input
	_ = parse(&in, r) // No error checking, since there is no required field

	user := r.Context().Value("user").(model.User)

	var notification model.Notification
	if database.DB.Where("user_uuid = ?", user.UUID).Where("id = ?", id).First(&notification).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Notification not found"))
		return
	}

	database.DB.Model(&notification).Update("read", in.Read)
	render.JSON(w, r, notification)
}

func DeleteNotification(w http.ResponseWriter, r *http.Request) {
	idString := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Notification not found"))
		return
	}

	user := r.Context().Value("user").(model.User)

	var notification model.Notification
	if database.DB.Where("user_uuid = ?", user.UUID).Where("id = ?", id).First(&notification).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Notification not found"))
		return
	}

	database.DB.Delete(&notification)
	w.WriteHeader(204)
}
