package controller

import (
	"bytes"
	"fmt"
	"github.com/spf13/viper"
	"golang.org/x/exp/slices"
	"io"
	"net/http"
	"os"
	"single-sign-on/auth"
	"single-sign-on/database"
	"single-sign-on/httperror"
	"single-sign-on/mailer"
	"single-sign-on/model"
	"single-sign-on/policy"
	"single-sign-on/util"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/gofrs/uuid"
)

type passwordData struct {
	OldPassword string `json:"oldPassword" validate:"nonzero"`
	NewPassword string `json:"newPassword" validate:"nonzero, min=8"`
}

type limitedUserView struct {
	UUID      uuid.UUID `json:"uuid"`
	Firstname string    `json:"firstname"`
	Lastname  string    `json:"lastname"`
	ImageURL  *string   `json:"imageURL"`
}

type newUserData struct {
	Firstname string     `json:"firstname"`
	Lastname  string     `json:"lastname"`
	Email     string     `json:"email"`
	Password  string     `json:"password"`
	Role      model.Role `json:"role"`
}

func AllUsers(w http.ResponseWriter, r *http.Request) {
	var users []model.User
	if val, ok := r.URL.Query()["uuid"]; ok {
		database.DB.Where("uuid in (?)", val).Find(&users)
	} else {
		database.DB.Find(&users)
	}

	if policy.AllUsersExtendedView(r) {
		for i := range users {
			database.DB.First(&users[i].Role, users[i].RoleID)
		}
		render.JSON(w, r, users)
	} else {
		res := make([]limitedUserView, len(users))

		for i := range users {
			res[i] = limitedUserView{
				UUID:      users[i].UUID,
				Firstname: users[i].Firstname,
				Lastname:  users[i].Lastname,
				ImageURL:  users[i].ImageURL,
			}
		}
		render.JSON(w, r, res)
	}
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	if !policy.GetUser() {
		notAuthorized(w, r)
		return
	}

	uuidString := chi.URLParam(r, "uuid")
	_uuid, err := uuid.FromString(uuidString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	var user model.User
	if database.DB.Where("uuid = ?", _uuid).First(&user).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	render.JSON(w, r, limitedUserView{UUID: user.UUID, Firstname: user.Firstname, Lastname: user.Lastname, ImageURL: user.ImageURL})
}

func Me(w http.ResponseWriter, r *http.Request) {
	if !policy.Me(r) {
		notAuthorized(w, r)
		return
	}
	user := r.Context().Value("user").(model.User)
	render.JSON(w, r, user.Without("blockedAt"))
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
	if !policy.CreateUser(r) {
		notAuthorized(w, r)
		return
	}

	var input newUserData
	err := parse(&input, r)
	if err != nil {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("One or more attributes are missing"))
		return
	}

	if database.DB.First(&model.Role{}, input.Role.ID).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Role not found"))
		return
	}

	if !database.DB.Where("email = ?", input.Email).Find(&model.User{}).RecordNotFound() {
		render.Status(r, 409)
		render.JSON(w, r, httperror.New("Email already registered"))
		return
	}

	user := model.User{
		Firstname: input.Firstname,
		Lastname:  input.Lastname,
		Email:     input.Email,
		RoleID:    input.Role.ID,
	}

	database.DB.Create(&user)

	preferences := model.DefaultPreferences()
	preferences.UserID = user.ID
	database.DB.Create(&preferences)
	user.Preferences = preferences

	token, err := util.GenerateToken(18)
	if err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Could not generate token"))
		return
	}
	user.PasswordResetToken = new(string)
	*user.PasswordResetToken = util.GetSHA256Sum(token)
	user.PasswordResetTimestamp = new(int64)
	*user.PasswordResetTimestamp = time.Now().Unix()
	database.DB.Save(&user)

	if err = mailer.SendUserCreatedEmail(user, token); err != nil {
		render.Status(r, 503)
		render.JSON(w, r, httperror.New("Could not send email"))
		return
	}

	render.JSON(w, r, user)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	uuidString := chi.URLParam(r, "uuid")
	_uuid, err := uuid.FromString(uuidString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	if !policy.UpdateUser(r, _uuid) {
		notAuthorized(w, r)
		return
	}

	var user model.User
	if database.DB.Where("uuid = ?", _uuid).First(&user).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	var updatedUser model.User
	err = render.DecodeJSON(r.Body, &updatedUser)
	if err != nil {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("Invalid user"))
		return
	}

	if !database.DB.Model(&user).Where("email = ?", updatedUser.Email).Where("uuid != ?", updatedUser.UUID).Find(&model.User{}).RecordNotFound() {
		render.Status(r, 409)
		render.JSON(w, r, httperror.New("Email address already registered"))
		return
	}

	omittedFields := []string{"ID", "Password", "CreatedAt", "UpdatedAt", "DeletedAt", "UUID", "BlockedAt"}
	if !policy.UpdateUserRole(r) {
		omittedFields = append(omittedFields, "Role")
	}

	database.DB.Model(&user).Omit(omittedFields...).Updates(updatedUser)
	render.JSON(w, r, user.Without("blockedAt"))
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	uuidString := chi.URLParam(r, "uuid")
	_uuid, err := uuid.FromString(uuidString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	if !policy.DeleteUser(r, _uuid) {
		notAuthorized(w, r)
		return
	}

	var user model.User
	if database.DB.Where("uuid = ?", _uuid).First(&user).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	//TODO: soft delete or hard delete?
	database.DB.Delete(&user)
	w.WriteHeader(204)
}

func UpdatePassword(w http.ResponseWriter, r *http.Request) {
	uuidString := chi.URLParam(r, "uuid")
	_uuid, err := uuid.FromString(uuidString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	if !policy.UpdatePassword(r, _uuid) {
		notAuthorized(w, r)
		return
	}

	var user model.User
	if database.DB.Where("uuid = ?", _uuid).First(&user).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	var data passwordData
	err = parse(&data, r)
	if err != nil {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("One or more attributes missing"))
		return
	}

	if !isValidPassword(data.NewPassword) {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("New password does not meet the password requirements"))
		return
	}

	match, err := auth.CheckPassword(data.OldPassword, *user.Password)
	if err != nil || !match {
		render.Status(r, 409)
		render.JSON(w, r, httperror.New("Old password incorrect"))
		return
	}

	hashedPassword, err := auth.HashPassword(data.NewPassword)
	if err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot hash password"))
		return
	}

	database.DB.Model(&user).Update("password", hashedPassword)
	w.WriteHeader(204)
}

func UpdateImage(w http.ResponseWriter, r *http.Request) {
	uuidString := chi.URLParam(r, "uuid")
	_uuid, err := uuid.FromString(uuidString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	if !policy.UpdateProfileImage(r, _uuid) {
		notAuthorized(w, r)
		return
	}

	var user model.User
	if database.DB.Where("uuid = ?", _uuid).First(&user).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	var buf bytes.Buffer
	defer buf.Reset()

	r.Body = http.MaxBytesReader(w, r.Body, 5*1024*1024) // 5 MB

	file, handler, err := r.FormFile("image")
	if err != nil {
		render.Status(r, 413)
		render.JSON(w, r, httperror.New("Image file size too large"))
		return
	}
	defer func() {
		_ = file.Close()
	}()

	parts := strings.Split(handler.Filename, ".")
	fileExtension := parts[len(parts)-1]
	_, err = io.Copy(&buf, file)
	if err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot copy image"))
		return
	}

	allowedContentTypes := []string{"image/png", "image/jpeg"}
	contentType := http.DetectContentType(buf.Bytes())
	if !slices.Contains(allowedContentTypes, contentType) {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("Invalid image"))
		return
	}

	filename := fmt.Sprintf("%s.%s", user.UUID.String(), fileExtension)
	path := viper.GetString("IMAGE_BASE_PATH") + "/" + filename
	err = os.WriteFile(path, buf.Bytes(), 0644)
	if err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot save image"))
		return
	}

	imageURL := "/static/images/" + filename
	database.DB.Model(&user).Update("image_url", imageURL)

	res := make(map[string]string)
	res["imageURL"] = imageURL
	render.JSON(w, r, res)
}

func DeleteImage(w http.ResponseWriter, r *http.Request) {
	uuidString := chi.URLParam(r, "uuid")
	_uuid, err := uuid.FromString(uuidString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	if !policy.DeleteProfileImage(r, _uuid) {
		notAuthorized(w, r)
		return
	}

	var user model.User
	if database.DB.Where("uuid = ?", _uuid).First(&user).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	err = os.Remove(viper.GetString("IMAGE_BASE_PATH") + strings.TrimPrefix(*user.ImageURL, "/static/images"))
	if err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot delete image"))
		return
	}

	database.DB.Model(&user).Update("image_url", nil)
	w.WriteHeader(204)
}

func BlockUser(w http.ResponseWriter, r *http.Request) {
	uuidString := chi.URLParam(r, "uuid")
	_uuid, err := uuid.FromString(uuidString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	if !policy.BlockUser(r) {
		notAuthorized(w, r)
		return
	}

	var user model.User
	if database.DB.Where("uuid = ?", _uuid).First(&user).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	if user.BlockedAt != nil {
		render.Status(r, 409)
		render.JSON(w, r, httperror.New("User is blocked"))
		return
	}

	database.DB.Model(&user).Update("BlockedAt", time.Now().Unix())
	w.WriteHeader(204)
}

func UnblockUser(w http.ResponseWriter, r *http.Request) {
	uuidString := chi.URLParam(r, "uuid")
	_uuid, err := uuid.FromString(uuidString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	if !policy.BlockUser(r) {
		notAuthorized(w, r)
		return
	}

	var user model.User
	if database.DB.Where("uuid = ?", _uuid).First(&user).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	if user.BlockedAt == nil {
		render.Status(r, 409)
		render.JSON(w, r, httperror.New("User is not blocked"))
		return
	}

	database.DB.Model(&user).Update("BlockedAt", nil)
	w.WriteHeader(204)
}

func GetPreferences(w http.ResponseWriter, r *http.Request) {
	uuidString := chi.URLParam(r, "uuid")
	_uuid, err := uuid.FromString(uuidString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	if !policy.GetPreferences(r, _uuid) {
		notAuthorized(w, r)
		return
	}

	var user model.User
	if database.DB.Where("uuid = ?", _uuid).First(&user).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	render.JSON(w, r, user.Preferences)
}

func UpdatePreferences(w http.ResponseWriter, r *http.Request) {
	uuidString := chi.URLParam(r, "uuid")
	_uuid, err := uuid.FromString(uuidString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	if !policy.UpdatePreferences(r, _uuid) {
		notAuthorized(w, r)
		return
	}

	var user model.User
	if database.DB.Where("uuid = ?", _uuid).First(&user).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("User not found"))
		return
	}

	var newPrefs model.Preferences
	err = parse(&newPrefs, r)

	if err != nil {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("Invalid preferences"))
		return
	}

	var prefs model.Preferences
	database.DB.Where("user_id = ?", user.ID).First(&prefs)
	prefs.DarkMode = newPrefs.DarkMode
	prefs.DefaultLanguage = newPrefs.DefaultLanguage
	database.DB.Save(&prefs)

	render.JSON(w, r, prefs)
}

func Search(w http.ResponseWriter, r *http.Request) {
	val, ok := r.URL.Query()["term"]
	if !ok {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("Term missing"))
		return
	}

	term := "%" + val[0] + "%"
	var users []model.User
	database.DB.Where("firstname LIKE ?", term).Or("lastname LIKE ?", term).Order("firstname").Order("lastname").Find(&users)

	res := make([]limitedUserView, len(users))
	for i, u := range users {
		res[i] = limitedUserView{UUID: u.UUID, Firstname: u.Firstname, Lastname: u.Lastname, ImageURL: u.ImageURL}
	}
	render.JSON(w, r, res)
}
