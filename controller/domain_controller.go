package controller

import (
	"bytes"
	"fmt"
	"github.com/gofrs/uuid"
	"github.com/spf13/viper"
	"golang.org/x/exp/slices"
	"io"
	"net/http"
	"os"
	"single-sign-on/database"
	"single-sign-on/httperror"
	"single-sign-on/model"
	"single-sign-on/policy"
	"single-sign-on/util"
	"strconv"
	"strings"

	"github.com/go-chi/chi"

	"github.com/go-chi/render"
)

type domainInput struct {
	ID                uint     `json:"id"`
	Name              string   `json:"name" validate:"nonzero"`
	Key               string   `json:"key" validate:"nonzero"`
	URL               string   `json:"url" validate:"nonzero"`
	NotificationToken *string  `json:"notificationToken"`
	UserAccess        []string `json:"userAccess"`
}

type extendedDomainView struct {
	ID         uint     `json:"id"`
	Key        string   `json:"key"`
	Name       string   `json:"name"`
	URL        string   `json:"url"`
	ImageURL   *string  `json:"imageURL"`
	UserAccess []string `json:"userAccess"`
}

func extendedDomainViewFrom(domain model.Domain) extendedDomainView {
	userAccess := make([]string, len(domain.UserAccess))
	for i := range domain.UserAccess {
		userAccess[i] = domain.UserAccess[i].UUID.String()
	}

	return extendedDomainView{
		ID:         domain.ID,
		Key:        domain.Key,
		Name:       domain.Name,
		URL:        domain.URL,
		ImageURL:   domain.ImageURL,
		UserAccess: userAccess,
	}
}

func AllDomains(w http.ResponseWriter, r *http.Request) {
	if !policy.AllDomains(r) {
		notAuthorized(w, r)
		return
	}

	if policy.AllDomainsExtended(r) {
		var domains []model.Domain
		database.DB.Find(&domains)
		extendedDomains := make([]extendedDomainView, len(domains))
		for i, d := range domains {
			extendedDomains[i] = extendedDomainViewFrom(d)
		}
		render.JSON(w, r, extendedDomains)
	} else {
		user := r.Context().Value("user").(model.User)
		domains := user.DomainAccess
		render.JSON(w, r, domains)
	}
}

func GetDomain(w http.ResponseWriter, r *http.Request) {
	if !policy.GetDomain(r) {
		notAuthorized(w, r)
		return
	}

	idString := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Domain not found"))
		return
	}

	if policy.GetDomainExtended(r) {
		var domain model.Domain
		if database.DB.First(&domain, id).RecordNotFound() {
			render.Status(r, 404)
			render.JSON(w, r, httperror.New("Domain not found"))
			return
		}
		render.JSON(w, r, extendedDomainViewFrom(domain))
	} else {
		user := r.Context().Value("user").(model.User)
		domainIndex := slices.IndexFunc(user.DomainAccess, func(domain *model.Domain) bool {
			return domain.ID == uint(id)
		})
		if domainIndex == -1 {
			render.Status(r, 404)
			render.JSON(w, r, httperror.New("Domain not found"))
			return
		}
		render.JSON(w, r, user.DomainAccess[domainIndex])
	}
}

func CreateDomain(w http.ResponseWriter, r *http.Request) {
	if !policy.CreateDomain(r) {
		notAuthorized(w, r)
		return
	}

	var input domainInput
	err := parse(&input, r)

	if err != nil {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("One or more attributes are missing"))
		return
	}

	if !database.DB.Where("`key` = ?", input.Key).Find(&model.Domain{}).RecordNotFound() {
		render.Status(r, 409)
		render.JSON(w, r, httperror.New("Key already in use"))
		return
	}

	userAccess := make([]*model.User, len(input.UserAccess))
	for i := range input.UserAccess {
		var user model.User
		if database.DB.Where("uuid = ?", input.UserAccess[i]).First(&user).RecordNotFound() {
			render.Status(r, 409)
			render.JSON(w, r, httperror.New("Unknown user"))
			return
		}
		userAccess[i] = &user
	}

	domain := model.Domain{
		Name:              input.Name,
		Key:               input.Key,
		URL:               input.URL,
		NotificationToken: input.NotificationToken,
		UserAccess:        userAccess,
	}

	database.DB.Create(&domain)
	render.JSON(w, r, extendedDomainViewFrom(domain))
}

func UpdateDomain(w http.ResponseWriter, r *http.Request) {
	if !policy.UpdateDomain(r) {
		notAuthorized(w, r)
		return
	}

	idString := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Domain not found"))
		return
	}

	var domain model.Domain
	if database.DB.First(&domain, id).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Domain not found"))
		return
	}

	var input domainInput
	err = render.DecodeJSON(r.Body, &input)
	if err != nil {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("Invalid domain"))
		return
	}

	if !database.DB.Where("`key` = ?", input.Key).Where("id != ?", id).Find(&model.Domain{}).RecordNotFound() {
		render.Status(r, 409)
		render.JSON(w, r, httperror.New("Key already in use"))
		return
	}

	domain.Name = input.Name
	domain.Key = input.Key
	domain.URL = input.URL
	if input.NotificationToken != nil {
		domain.NotificationToken = new(string)
		*domain.NotificationToken = util.GetSHA256Sum(*input.NotificationToken)
	}

	userAccess := make([]*model.User, len(input.UserAccess))
	for i := range input.UserAccess {
		var user model.User
		if database.DB.Where("uuid = ?", input.UserAccess[i]).First(&user).RecordNotFound() {
			render.Status(r, 409)
			render.JSON(w, r, httperror.New("Unknown user"))
			return
		}
		userAccess[i] = &user
	}

	database.DB.Save(&domain)
	database.DB.Model(&domain).Association("UserAccess").Replace(userAccess)
	render.JSON(w, r, extendedDomainViewFrom(domain))
}

func DeleteDomain(w http.ResponseWriter, r *http.Request) {
	if !policy.DeleteDomain(r) {
		notAuthorized(w, r)
		return
	}

	idString := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Domain not found"))
		return
	}

	var domain model.Domain
	if database.DB.First(&domain, id).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Domain not found"))
		return
	}

	database.DB.Delete(&domain)
	w.WriteHeader(204)
}

func UpdateDomainImage(w http.ResponseWriter, r *http.Request) {
	if !policy.UpdateDomainImage(r) {
		notAuthorized(w, r)
		return
	}

	idString := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Domain not found"))
		return
	}

	var domain model.Domain
	if database.DB.First(&domain, id).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Domain not found"))
		return
	}

	var buf bytes.Buffer
	defer buf.Reset()

	r.Body = http.MaxBytesReader(w, r.Body, 5*1024*1024) // 5 MB
	file, handler, err := r.FormFile("image")
	if err != nil {
		render.Status(r, 413)
		render.JSON(w, r, httperror.New("Image file size too large"))
		return
	}
	defer func() {
		_ = file.Close()
	}()

	_, err = io.Copy(&buf, file)
	if err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot copy image"))
		return
	}

	allowedContentTypes := []string{"image/png", "image/jpeg"}
	contentType := http.DetectContentType(buf.Bytes())
	if !slices.Contains(allowedContentTypes, contentType) {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("Invalid image"))
		return
	}

	fileUuid, err := uuid.NewV4()
	if err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot generate UUID"))
		return
	}

	parts := strings.Split(handler.Filename, ".")
	fileExtension := parts[len(parts)-1]
	filename := fmt.Sprintf("%s.%s", fileUuid.String(), fileExtension)
	path := fmt.Sprintf("%s/%s", viper.GetString("IMAGE_BASE_PATH"), filename)

	err = os.WriteFile(path, buf.Bytes(), 0644)
	if err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot save image"))
		return
	}

	// Delete old image
	oldImageURL := domain.ImageURL
	if oldImageURL != nil {
		_ = os.Remove(viper.GetString("IMAGE_BASE_PATH") + strings.TrimPrefix(*oldImageURL, "/static/images"))
	}

	// Save new image
	imageURL := "/static/images/" + filename
	database.DB.Model(&domain).Update("image_url", imageURL)

	res := make(map[string]string)
	res["imageURL"] = imageURL
	render.JSON(w, r, res)
}

func DeleteDomainImage(w http.ResponseWriter, r *http.Request) {
	if !policy.DeleteDomainImage(r) {
		notAuthorized(w, r)
		return
	}

	idString := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idString)
	if err != nil {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Domain not found"))
		return
	}

	var domain model.Domain
	if database.DB.First(&domain, id).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Domain not found"))
		return
	}

	err = os.Remove(viper.GetString("IMAGE_BASE_PATH") + strings.TrimPrefix(*domain.ImageURL, "/static/images"))
	if err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot delete image"))
		return
	}

	database.DB.Model(&domain).Update("image_url", nil)
	w.WriteHeader(204)
}
