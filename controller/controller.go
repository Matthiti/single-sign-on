package controller

import (
	"net/http"
	"single-sign-on/httperror"

	"github.com/go-chi/render"
	"gopkg.in/validator.v2"
)

func parse(model interface{}, r *http.Request) error {
	err := render.DecodeJSON(r.Body, &model)

	if err != nil {
		return err
	}
	err = validator.Validate(model)
	return err
}

func notAuthorized(w http.ResponseWriter, r *http.Request) {
	render.Status(r, 403)
	render.JSON(w, r, httperror.New("Not authorized to do this action"))
}
