package controller

import (
	"net/http"
	"single-sign-on/auth"
	"single-sign-on/database"
	"single-sign-on/httperror"
	"single-sign-on/mailer"
	"single-sign-on/model"
	"single-sign-on/util"
	"strings"
	"time"
	"unicode"

	"github.com/go-chi/render"
)

type credentials struct {
	Email      string `json:"email" validate:"nonzero"`
	Password   string `json:"password" validate:"nonzero"`
	RememberMe bool   `json:"rememberMe"`
}

type registerData struct {
	Firstname   string `json:"firstname" validate:"nonzero"`
	Lastname    string `json:"lastname" validate:"nonzero"`
	Email       string `json:"email" validate:"nonzero"`
	Password    string `json:"password" validate:"nonzero"`
	InviteToken string `json:"inviteToken" validate:"nonzero"`
}

type refreshData struct {
	RefreshToken string `json:"refreshToken" validate:"nonzero"`
}

func Login(w http.ResponseWriter, r *http.Request) {
	var credentials credentials
	err := parse(&credentials, r)

	if err != nil {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("Email or password missing"))
		return
	}

	var user model.User
	if database.DB.Where("email = ?", strings.ToLower(credentials.Email)).First(&user).RecordNotFound() {
		render.Status(r, 401)
		render.JSON(w, r, httperror.New("Email or password incorrect"))
		return
	}

	if user.Password == nil {
		render.Status(r, 409)
		render.JSON(w, r, httperror.New("Your account is not activated"))
		return
	}

	match, err := auth.CheckPassword(credentials.Password, *user.Password)

	if err != nil || !match {
		render.Status(r, 401)
		render.JSON(w, r, httperror.New("Email or password incorrect"))
		return
	}

	if user.BlockedAt != nil {
		render.Status(r, 403)
		render.JSON(w, r, httperror.New("Your account has been blocked"))
		return
	}

	token, err := auth.GenerateJWTToken(user)
	if err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot generate JWT token"))
		return
	}

	response := make(map[string]string)
	response["token"] = token
	if credentials.RememberMe {
		if response["refreshToken"], err = generateRememberToken(&user); err != nil {
			render.Status(r, 500)
			render.JSON(w, r, httperror.New("Cannot generate refresh token"))
			return
		}
	}

	render.JSON(w, r, response)
}

func Register(w http.ResponseWriter, r *http.Request) {
	var data registerData
	err := parse(&data, r)
	data.Email = strings.ToLower(data.Email)

	if err != nil {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("One or more attributes missing"))
		return
	}

	hash := util.GetSHA256Sum(data.InviteToken)

	var inviteToken model.InviteToken
	if database.DB.Where("token = ?", hash).Find(&inviteToken).RecordNotFound() {
		render.Status(r, 401)
		render.JSON(w, r, httperror.New("Invalid invite token"))
		return
	}

	if !database.DB.Where("email = ?", data.Email).Find(&model.User{}).RecordNotFound() {
		render.Status(r, 409)
		render.JSON(w, r, httperror.New("Email already registered"))
		return
	}

	if !isValidPassword(data.Password) {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("Password does not meet the password requirements"))
		return
	}

	hashedPassword, err := auth.HashPassword(data.Password)
	if err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot hash password"))
		return
	}

	database.DB.Delete(&inviteToken)

	user := model.User{
		Firstname: data.Firstname,
		Lastname:  data.Lastname,
		Email:     data.Email,
		Password:  &hashedPassword,
		RoleID:    inviteToken.RoleID,
	}

	database.DB.Create(&user)

	preferences := model.DefaultPreferences()
	preferences.UserID = user.ID
	database.DB.Create(&preferences)

	token, err := auth.GenerateJWTToken(user)
	if err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot generate JWT token"))
		return
	}

	response := make(map[string]string)
	response["token"] = token
	render.JSON(w, r, response)
}

func Refresh(w http.ResponseWriter, r *http.Request) {
	var data refreshData

	err := parse(&data, r)
	if err != nil {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("Refresh token missing"))
		return
	}

	var user model.User
	if database.DB.Where("refresh_token = ?", util.GetSHA256Sum(data.RefreshToken)).First(&user).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Invalid refresh token"))
		return
	}

	response := make(map[string]string)
	if response["token"], err = auth.GenerateJWTToken(user); err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot generate JWT token"))
		return
	}

	render.JSON(w, r, response)
}

func RequestPasswordReset(w http.ResponseWriter, r *http.Request) {
	type input struct {
		Email string `json:"email" validate:"nonzero"`
	}

	var in input
	err := parse(&in, r)
	if err != nil {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("Email missing"))
		return
	}
	in.Email = strings.ToLower(in.Email)

	var user model.User
	if database.DB.Where("email = ?", in.Email).First(&user).RecordNotFound() {
		if err = mailer.SendPasswordResetUnknownEmail(in.Email); err != nil {
			render.Status(r, 503)
			render.JSON(w, r, httperror.New("Could not send email"))
			return
		}
	} else {
		token, err := util.GenerateToken(18)
		if err != nil {
			render.Status(r, 500)
			render.JSON(w, r, httperror.New("Could not generate token"))
			return
		}
		user.PasswordResetToken = new(string)
		*user.PasswordResetToken = util.GetSHA256Sum(token)
		user.PasswordResetTimestamp = new(int64)
		*user.PasswordResetTimestamp = time.Now().Unix()
		database.DB.Save(&user)

		if err = mailer.SendPasswordResetToken(user, token); err != nil {
			render.Status(r, 503)
			render.JSON(w, r, httperror.New("Could not send email"))
			return
		}
	}
	w.WriteHeader(201)
}

func ResetPassword(w http.ResponseWriter, r *http.Request) {
	type input struct {
		Token    string `json:"token" validate:"nonzero"`
		Password string `json:"password" validate:"nonzero"`
	}

	var in input
	err := parse(&in, r)
	if err != nil {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("One or more attributes missing"))
		return
	}

	if !isValidPassword(in.Password) {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("Password does not meet the password requirements"))
		return
	}

	var user model.User
	if database.DB.Where("password_reset_token = ?", util.GetSHA256Sum(in.Token)).First(&user).RecordNotFound() || time.Now().Unix()-*user.PasswordResetTimestamp > 60*60 {
		render.Status(r, 409)
		render.JSON(w, r, httperror.New("Invalid token"))
		return
	}

	hashedPassword, err := auth.HashPassword(in.Password)
	if err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot hash password"))
		return
	}

	user.Password = &hashedPassword
	user.PasswordResetToken = nil
	user.PasswordResetTimestamp = nil
	database.DB.Save(&user)

	response := make(map[string]string)
	if response["token"], err = auth.GenerateJWTToken(user); err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Cannot generate JWT token"))
		return
	}

	render.JSON(w, r, response)
}

func isValidPassword(password string) bool {
	if len(password) < 8 {
		return false
	}
	var hasDigit, hasUppercase, hasLowercase, hasSpecial bool
	for _, c := range password {
		switch {
		case unicode.IsNumber(c):
			hasDigit = true
		case unicode.IsUpper(c):
			hasUppercase = true
		case unicode.IsLower(c):
			hasLowercase = true
		case unicode.IsPunct(c) || unicode.IsSymbol(c):
			hasSpecial = true
		}
	}
	return hasDigit && hasUppercase && hasLowercase && hasSpecial
}

func generateRememberToken(user *model.User) (string, error) {
	token, err := util.GenerateToken(50)
	if err != nil {
		return "", err
	}
	user.RefreshToken = util.GetSHA256Sum(token)
	database.DB.Save(&user)

	return token, nil
}
