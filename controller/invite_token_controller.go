package controller

import (
	"net/http"
	"single-sign-on/database"
	"single-sign-on/httperror"
	"single-sign-on/mailer"
	"single-sign-on/model"
	"single-sign-on/policy"
	"single-sign-on/util"

	"github.com/go-chi/render"
)

type inviteTokenData struct {
	Email  string `json:"email" validate:"nonzero"`
	RoleID uint   `json:"roleID" validate:"nonzero"`
}

func CreateInviteToken(w http.ResponseWriter, r *http.Request) {
	if !policy.CreateInviteToken(r) {
		notAuthorized(w, r)
		return
	}

	var data inviteTokenData
	err := parse(&data, r)
	if err != nil {
		render.Status(r, 400)
		render.JSON(w, r, httperror.New("One or more attributes are missing"))
		return
	}

	if database.DB.First(&model.Role{}, data.RoleID).RecordNotFound() {
		render.Status(r, 404)
		render.JSON(w, r, httperror.New("Role not found"))
		return
	}

	token, err := util.GenerateToken(18)
	if err != nil {
		render.Status(r, 500)
		render.JSON(w, r, httperror.New("Could not generate random token"))
		return
	}

	hash := util.GetSHA256Sum(token)
	inviteToken := model.InviteToken{Token: hash, RoleID: data.RoleID}
	database.DB.Create(&inviteToken)

	err = mailer.SendInvite(data.Email, token)
	if err != nil {
		render.Status(r, 503)
		render.JSON(w, r, httperror.New("Could not send email"))
		return
	}
	w.WriteHeader(201)
}
