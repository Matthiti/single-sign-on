package controller

import (
	"net/http"
	"single-sign-on/database"
	"single-sign-on/model"
	"single-sign-on/policy"

	"github.com/go-chi/render"
)

func AllRoles(w http.ResponseWriter, r *http.Request) {
	if !policy.AllRoles(r) {
		notAuthorized(w, r)
		return
	}
	var roles []model.Role
	database.DB.Find(&roles)
	render.JSON(w, r, roles)
}
