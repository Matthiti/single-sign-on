package mailer

import (
	"fmt"
	"github.com/spf13/viper"
	"single-sign-on/model"
	"time"

	"github.com/matcornic/hermes/v2"
	"gopkg.in/gomail.v2"
)

var translations = map[string]map[string]string{
	"en": {
		"copyright":                    fmt.Sprintf("Copyright © %d roelink.eu. All rights reserved.", time.Now().Year()),
		"greeting":                     "Hi",
		"signature":                    "Regards",
		"outro":                        "Need help, or have questions? Send an email to support@roelink.eu.",
		"troubleText":                  "If the {ACTION}-button is not working for you, just copy and paste the URL below into your web browser.",
		"invite.subject":               "You have been invited to register at login.roelink.eu.",
		"invite.intro":                 "You have been invited to register at login.roelink.eu.",
		"invite.instructions":          "To get started, please click here:",
		"invite.button":                "Register",
		"passwordReset.subject":        "Password reset at login.roelink.eu.",
		"passwordReset.intro":          "You requested a password reset at login.roelink.eu.",
		"passwordReset.instructions":   "You can reset your password by clicking the button below:",
		"passwordReset.button":         "Reset password",
		"passwordResetUnknown.subject": "Password reset at login.roelink.eu.",
		"passwordResetUnknown.intro":   "You requested a password reset at login.roelink.eu. Unfortunately, this email address is not registered.",
		"newNotification.subject":      "New notification at ",
		"newNotification.title":        "Title",
		"newNotification.content":      "Content",
		"newNotification.intro":        "You received a new notification at ",
		"newNotification.instructions": "To view all your notifications, please click here:",
		"newNotification.button":       "View notifications",
		"newAccount.subject":           "New account at login.roelink.eu.",
		"newAccount.intro":             "An administrator has created an account for you at login.roelink.eu.",
		"newAccount.instructions":      "The email address associated to your account is the email address on which you received this email. You can set a password by clicking the button below:",
		"newAccount.button":            "Set password",
	},
	"nl": {
		"copyright":                    fmt.Sprintf("Copyright © %d roelink.eu. Alle rechten voorbehouden.", time.Now().Year()),
		"greeting":                     "Hallo",
		"signature":                    "Met vriendelijke groet",
		"outro":                        "Hulp nodig of vragen? Stuur een email naar support@roelink.eu.",
		"troubleText":                  "Als de {ACTION}-knop niet werkt, kopieer en plak de onderstaande URL in uw browser.",
		"invite.subject":               "U bent uitgenodigd om u te registeren op login.roelink.eu.",
		"invite.intro":                 "U bent uitgenodigd om u te registeren op login.roelink.eu.",
		"invite.instructions":          "Klik hier om the beginnen:",
		"invite.button":                "Registreren",
		"passwordReset.subject":        "Wachtwoord vergeten op login.roelink.eu.",
		"passwordReset.intro":          "U heeft een wachtwoordherstel aangevraagd op login.roelink.eu.",
		"passwordReset.instructions":   "Om uw wachtwoord te herstellen, klik op onderstaande knop:",
		"passwordReset.button":         "Herstel wachtwoord",
		"passwordResetUnknown.subject": "Wachtwoord vergeten op login.roelink.eu.",
		"passwordResetUnknown.intro":   "U heeft een wachtworodherstel aangevraagd op login.roelink.eu. Dit emailadres is helaas niet geregistreerd.",
		"newNotification.subject":      "Nieuwe melding op ",
		"newNotification.title":        "Titel",
		"newNotification.content":      "Inhoud",
		"newNotification.intro":        "U heeft een nieuwe melding ontvangen op ",
		"newNotification.instructions": "Om al uw meldingen te zien, klik hier:",
		"newNotification.button":       "Bekijk meldingen",
		"newAccount.subject":           "Nieuw account op login.roelink.eu",
		"newAccount.intro":             "Een administrator heeft een account voor u aangemaakt op login.roelink.eu.",
		"newAccount.instructions":      "Het emailadres van uw account is het email adres waar deze mail naartoe gestuurd is. U kunt uw wachtwoord instellen door op de onderstaande knop te klikken:",
		"newAccount.button":            "Stel wachtwoord in",
	},
}

func HermesFor(locale string) hermes.Hermes {
	h := hermes.Hermes{
		Product: hermes.Product{
			Name:      "roelink.eu",
			Link:      "https://login.roelink.eu",
			Copyright: "Copyright © 2019 roelink.eu. All rights reserved.",
			Logo:      "https://roelink.eu/icon/android-chrome-192x192.png",
		},
		Theme: new(hermes.Default),
	}
	h.Product.Copyright = translations[locale]["copyright"]
	h.Product.TroubleText = translations[locale]["troubleText"]
	return h
}

func defaultLanguage() string {
	return model.DefaultPreferences().DefaultLanguage
}

func SendInvite(to string, token string) error {
	url := "https://login.roelink.eu/register?invite_token=" + token
	subject := translations[defaultLanguage()]["invite.subject"]
	email := hermes.Email{
		Body: hermes.Body{
			Title: translations[defaultLanguage()]["greeting"] + ",",
			Intros: []string{
				translations[defaultLanguage()]["invite.intro"],
			},
			Actions: []hermes.Action{
				{
					Instructions: translations[defaultLanguage()]["invite.instructions"],
					Button: hermes.Button{
						Color: "#1976d2",
						Text:  translations[defaultLanguage()]["invite.button"],
						Link:  url,
					},
				},
			},
			Outros: []string{
				translations[defaultLanguage()]["outro"],
			},
			Signature: translations[defaultLanguage()]["signature"],
		},
	}
	return send(to, subject, HermesFor(defaultLanguage()), email)
}

func SendPasswordResetToken(user model.User, token string) error {
	url := "https://login.roelink.eu/reset-password?token=" + token
	subject := translations[user.Preferences.DefaultLanguage]["passwordReset.subject"]
	email := hermes.Email{
		Body: hermes.Body{
			Greeting: translations[user.Preferences.DefaultLanguage]["greeting"],
			Name:     user.Firstname,
			Intros: []string{
				translations[user.Preferences.DefaultLanguage]["passwordReset.intro"],
			},
			Actions: []hermes.Action{
				{
					Instructions: translations[user.Preferences.DefaultLanguage]["passwordReset.instructions"],
					Button: hermes.Button{
						Color: "#1976d2",
						Text:  translations[user.Preferences.DefaultLanguage]["passwordReset.button"],
						Link:  url,
					},
				},
			},
			Outros: []string{
				translations[user.Preferences.DefaultLanguage]["outro"],
			},
			Signature: translations[user.Preferences.DefaultLanguage]["signature"],
		},
	}
	return send(user.Email, subject, HermesFor(user.Preferences.DefaultLanguage), email)
}

func SendPasswordResetUnknownEmail(to string) error {
	subject := translations[defaultLanguage()]["passwordResetUnknown.subject"]
	email := hermes.Email{
		Body: hermes.Body{
			Title: translations[defaultLanguage()]["greeting"] + ",",
			Intros: []string{
				translations[defaultLanguage()]["passwordResetUnknown.intro"],
			},
			Outros: []string{
				translations[defaultLanguage()]["outro"],
			},
			Signature: translations[defaultLanguage()]["signature"],
		},
	}
	return send(to, subject, HermesFor(defaultLanguage()), email)
}

func SendNotification(user model.User, notification model.Notification) error {
	subject := translations[user.Preferences.DefaultLanguage]["newNotification.subject"] + notification.Domain.URL + "."
	dictionary := []hermes.Entry{
		{Key: translations[user.Preferences.DefaultLanguage]["newNotification.title"], Value: notification.Title},
	}

	if notification.Content != nil {
		dictionary = append(dictionary, hermes.Entry{Key: translations[user.Preferences.DefaultLanguage]["newNotification.content"], Value: *notification.Content})
	}

	email := hermes.Email{
		Body: hermes.Body{
			Greeting: translations[user.Preferences.DefaultLanguage]["greeting"],
			Name:     user.Firstname,
			Intros: []string{
				translations[user.Preferences.DefaultLanguage]["newNotification.intro"] + notification.Domain.URL + ":",
			},
			Dictionary: dictionary,
			Actions: []hermes.Action{
				{
					Instructions: translations[user.Preferences.DefaultLanguage]["newNotification.instructions"],
					Button: hermes.Button{
						Color: "#1976d2",
						Text:  translations[user.Preferences.DefaultLanguage]["newNotification.button"],
						Link:  notification.Domain.URL,
					},
				},
			},
			Outros: []string{
				translations[user.Preferences.DefaultLanguage]["outro"],
			},
			Signature: translations[user.Preferences.DefaultLanguage]["signature"],
		},
	}
	return send(user.Email, subject, HermesFor(user.Preferences.DefaultLanguage), email)
}

func SendUserCreatedEmail(user model.User, token string) error {
	url := "https://login.roelink.eu/reset-password?token=" + token
	subject := translations[user.Preferences.DefaultLanguage]["newAccount.subject"]
	email := hermes.Email{
		Body: hermes.Body{
			Greeting: translations[user.Preferences.DefaultLanguage]["greeting"],
			Name:     user.Firstname,
			Intros: []string{
				translations[user.Preferences.DefaultLanguage]["newAccount.intro"],
			},
			Actions: []hermes.Action{
				{
					Instructions: translations[user.Preferences.DefaultLanguage]["newAccount.instructions"],
					Button: hermes.Button{
						Color: "#1976d2",
						Text:  translations[user.Preferences.DefaultLanguage]["newAccount.button"],
						Link:  url,
					},
				},
			},
			Outros: []string{
				translations[user.Preferences.DefaultLanguage]["outro"],
			},
			Signature: translations[user.Preferences.DefaultLanguage]["signature"],
		},
	}
	return send(user.Email, subject, HermesFor(user.Preferences.DefaultLanguage), email)
}

func send(to string, subject string, h hermes.Hermes, email hermes.Email) error {
	emailBody, err := h.GenerateHTML(email)
	if err != nil {
		return err
	}

	emailText, err := h.GeneratePlainText(email)
	if err != nil {
		return err
	}

	m := gomail.NewMessage()
	m.SetHeader("From", "\"Roelink.eu Notifications\" <notifications@roelink.eu>")
	m.SetHeader("To", to)
	m.SetHeader("Subject", subject)

	m.SetBody("text/plain", emailText)
	m.AddAlternative("text/html", emailBody)

	port := viper.GetInt("MAIL_PORT")
	d := gomail.Dialer{Host: viper.GetString("MAIL_HOST"), Port: port, Username: viper.GetString("MAIL_USERNAME"), Password: viper.GetString("MAIL_PASSWORD"), SSL: !viper.GetBool("DEBUG")}
	return d.DialAndSend(m)
}
