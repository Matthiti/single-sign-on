package util

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
)

func GenerateToken(length int) (string, error) {
	rb := make([]byte, length)
	_, err := rand.Read(rb)
	if err != nil {
		return "", err
	}

	token := base64.URLEncoding.EncodeToString(rb)
	return token, nil
}

func GetSHA256Sum(token string) string {
	hasher := sha256.New()
	hasher.Write([]byte(token))
	hash := hex.EncodeToString(hasher.Sum(nil))
	return hash
}
