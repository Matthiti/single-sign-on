package model

import (
	// "single-sign-on/database"

	"encoding/json"

	"github.com/gofrs/uuid"
	"github.com/jinzhu/gorm"
)

type Role struct {
	ID   uint   `gorm:"primary_key" json:"id"`
	Name string `gorm:"unique; not null" json:"name"`
}

type User struct {
	gorm.Model             `json:"-"`
	Firstname              string      `gorm:"not null" json:"firstname" validate:"nonzero"`
	Lastname               string      `gorm:"not null" json:"lastname" validate:"nonzero"`
	Email                  string      `gorm:"unique;not null" json:"email" validate:"nonzero"`
	Password               *string     `json:"-" validate:"nonzero, min=8"`
	UUID                   uuid.UUID   `gorm:"unique" json:"uuid"`
	RoleID                 uint        `json:"-"` // https://github.com/jinzhu/gorm/issues/765
	Role                   Role        `json:"role"`
	ImageURL               *string     `json:"imageURL"`
	BlockedAt              *int64      `json:"blockedAt"`
	RefreshToken           string      `json:"-"`
	PasswordResetToken     *string     `json:"-"`
	PasswordResetTimestamp *int64      `json:"-"`
	Preferences            Preferences `json:"-"`
	DomainAccess           []*Domain   `gorm:"many2many:user_domain_access" json:"domainAccess"`
}

func (user *User) Without(attributes ...string) map[string]interface{} {
	jsonObject, _ := json.Marshal(user)

	var userMap map[string]interface{}
	_ = json.Unmarshal(jsonObject, &userMap)

	for _, attribute := range attributes {
		delete(userMap, attribute)
	}

	return userMap
}

type Domain struct {
	ID                uint    `gorm:"primary_key" json:"id"`
	Key               string  `gorm:"unique; not null" json:"key" validate:"nonzero"`
	Name              string  `gorm:"not null" json:"name" validate:"nonzero"`
	URL               string  `gorm:"not null" json:"url" validate:"nonzero"`
	NotificationToken *string `json:"-"`
	ImageURL          *string `json:"imageURL"`
	UserAccess        []*User `gorm:"many2many:user_domain_access" json:"-"`
}

type InviteToken struct {
	ID     uint   `gorm:"primary_key" json:"-"`
	Token  string `gorm:"unique" json:"-"`
	RoleID uint   `json:"-"`
	Role   Role   `json:"-"`
}

type Preferences struct {
	ID              uint   `gorm:"primary_key" json:"-"`
	UserID          uint   `json:"-"`
	DarkMode        bool   `json:"darkMode"`
	DefaultLanguage string `gorm:"default:'en'" json:"defaultLanguage" validate:"nonzero"`
}

func DefaultPreferences() Preferences {
	return Preferences{DarkMode: false, DefaultLanguage: "nl"}
}

type Notification struct {
	ID        uint      `gorm:"primary_key" json:"id"`
	UserUUID  uuid.UUID `gorm:"not null" json:"userUuid" validate:"nonzero"`
	CreatedAt int64     `gorm:"not null" json:"createdAt"`
	DomainID  uint      `gorm:"not null" json:"-"`
	Domain    Domain    `json:"domain" validate:"-"`
	Read      bool      `json:"read"`
	Title     string    `gorm:"not null" json:"title" validate:"nonzero"`
	Content   *string   `json:"content"`
}

func (user *User) BeforeCreate(scope *gorm.Scope) error {
	_uuid, err := uuid.NewV4()
	if err != nil {
		return err
	}
	return scope.SetColumn("UUID", _uuid)
}
