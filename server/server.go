package server

import (
	"fmt"
	"github.com/spf13/viper"
	"net/http"
	"single-sign-on/auth"
	"single-sign-on/controller"
	"single-sign-on/database"
	"single-sign-on/logger"
	"single-sign-on/middleware"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"
)

func Start(port uint) {
	err := database.InitConnection()
	if err != nil {
		logger.Fatal("Cannot connect to database", err)
	}

	err = auth.InitializeJWT()
	if err != nil {
		logger.Fatal("Cannot read public/private key pair", err)
	}

	r := routes()
	logger.Log(fmt.Sprintf("Listening on port %d", port))
	err = http.ListenAndServe(fmt.Sprintf(":%d", port), r)
	if err != nil {
		logger.Fatal(fmt.Sprintf("Could not listen on port %d", port), err)
	}
}

func routes() *chi.Mux {
	r := chi.NewRouter()

	corsConfig := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders: []string{"Accept", "X-Authorization", "Content-Type"},
		MaxAge:         300,
	})
	r.Use(corsConfig.Handler)
	r.Use(middleware.LogMiddleware)
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Use(middleware.JWTMiddleware)

	r.Route("/api/v1", func(r chi.Router) {
		r.Route("/auth", func(r chi.Router) {
			r.Post("/login", controller.Login)
			r.Post("/register", controller.Register)
			r.Post("/refresh", controller.Refresh)
			r.Post("/request_password_reset", controller.RequestPasswordReset)
			r.Post("/reset_password", controller.ResetPassword)
		})
		r.Get("/me", controller.Me)

		r.Route("/domains", func(r chi.Router) {
			r.Get("/", controller.AllDomains)
			r.Post("/", controller.CreateDomain)
			r.Route("/{id}", func(r chi.Router) {
				r.Get("/", controller.GetDomain)
				r.Put("/", controller.UpdateDomain)
				r.Delete("/", controller.DeleteDomain)
				r.Post("/image", controller.UpdateDomainImage)
				r.Delete("/image", controller.DeleteDomainImage)
			})
		})

		r.Route("/users", func(r chi.Router) {
			r.Get("/", controller.AllUsers)
			r.Post("/", controller.CreateUser)
			r.Route("/{uuid}", func(r chi.Router) {
				r.Get("/", controller.GetUser)
				r.Put("/", controller.UpdateUser)
				r.Delete("/", controller.DeleteUser)
				r.Put("/password", controller.UpdatePassword)
				r.Post("/image", controller.UpdateImage)
				r.Delete("/image", controller.DeleteImage)
				r.Put("/block", controller.BlockUser)
				r.Put("/unblock", controller.UnblockUser)
				r.Route("/preferences", func(r chi.Router) {
					r.Get("/", controller.GetPreferences)
					r.Put("/", controller.UpdatePreferences)
				})
			})
			r.Get("/search", controller.Search)
		})

		r.Post("/invite_tokens", controller.CreateInviteToken)

		r.Route("/roles", func(r chi.Router) {
			r.Get("/", controller.AllRoles)
		})

		r.Route("/notifications", func(r chi.Router) {
			r.Get("/", controller.AllNotifications)
			r.Post("/", controller.CreateNotification)
			r.Route("/{id}", func(r chi.Router) {
				r.Get("/", controller.GetNotification)
				r.Put("/", controller.ReadNotification)
				r.Delete("/", controller.DeleteNotification)
			})
		})
	})

	FileServer(r, "/static/images", http.Dir(viper.GetString("IMAGE_BASE_PATH")))
	return r
}

func FileServer(r chi.Router, path string, root http.Dir) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit URL parameters.")
	}

	fs := http.StripPrefix(path, http.FileServer(staticFS{root}))

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", 301).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, func(w http.ResponseWriter, r *http.Request) {
		fs.ServeHTTP(w, r)
	})
}

// https://stackoverflow.com/questions/51169677/http-static-file-handler-keeps-showing-the-directory-list
type staticFS struct {
	http.Dir
}

func (f staticFS) Open(name string) (result http.File, err error) {
	file, err := f.Dir.Open(name)
	if err != nil {
		return nil, err
	}

	fileStat, err := file.Stat()
	if err != nil {
		return nil, err
	}

	if fileStat.IsDir() {
		return f.Dir.Open("does-not-exist")
	}

	return file, nil
}
